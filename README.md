# Angular UIB Alert #

An Angular.js alert/confirm UIB bootstrap modal

Dependencies:

* Angular
* Angular Bootstrap
* Bootstrap 3 (css)

### Install with Bower ###

```html
sudo bower install https://bitbucket.org/busineyss/angular-uib-alert.git --save --allow-root
```

In your html/template add 

```html
<script src="/bower_components/angular-uib-alert/dist/angular-uib-alert.js"></script>
```

In your application, declare dependency injection like so:

```javascript
angular.module('myApp', ['angular-uib-alert']);
```

#### Methods ####

alert(message)

* message (String) : Message of the Alert dialog

confirm(message, buttonArray)

* message (String) : Message of the Confirm dialog
* buttonArray (Array) : Array of buttons to display. By default : ['OK', 'Cancel']

#### Examples ####

```javascript
module.controller('MyCtrl', function($scope, $uibAlert) {

  $uibAlert.alert('message')
    .then(function() {
      // callback success
    });

  $uibAlert.confirm('message', [{
        text: 'No',
        class: 'btn btn-default',
        value: false
    }, {
        text: 'Yes',
        class: 'btn btn-primary',
        value: true
    }]).then(function(response) {
        // callback success (return value of button)
        console.log(response);
    });

});
```